## Respostas ao teste "Sistemas do Secovi"

### Parte 2 - Prática

**1)** Arquivo **mapear.bat**

**2)** Arquivo **busca_especial.sh**

**Obs:** Pode ser usado o arquivo de testes especial.txt

**3)** Arquivo **data_atual.js**

**4)** Pulei, pois estou com uma grande dificuldade em fazer a Expressão Regular ou função recursiva para resolver

**5)** Arquivo **carros.sql**

**6)** Arquivos:

**pessoas.sql**

**contatos.sql**

**enderecos.sql**

**insert_pessoas.sql**

**insert_contatos.sql**

**insert_contato_endereco.sql**

Não fiz a consulta, pois tive dificuldades em fazer o **INNER JOIN**

**7)** Arquivo **ler_contatos.php**

**Obs:** Requer o arquivo **crud.php**

**8)** Arquivo **form_cadastrio.php**

**Obs:** Requer o arquivo **crud.php**

**9)** Requisitos mínimos:

Linux 
Apache ou Nguinx
PHP (Usei a versão 7.4.16) [Acredito que funcione em qualquer versão 7 e no 8]
Banco Postgress versão 12

Para testar as repostas do item **6**, basta ter instalado o postgres e rodar o comando:

sudo -u postgres psql -d secovi -c "**Conteúdo do Arquivo .sql**"

Bastando abrir o Arquivo .sql no vi em qualquer editor ou dar um **cat** nele e mantendo a ordem aqui apresentada

Ou caso queira pode instalar o PgAdmin e fazer via navegador

Para testar as repostas do item **7** é nescessário colocar os arquivos .php na pasta "default" do Apache ou Nguinx

E abrir a URL http://127.0.0.1/'Nome do Arquivo .php'

**10)** Pulei, pois estou com uma grande dificuldade em fazer a Expressão Regular ou função recursiva para resolver







