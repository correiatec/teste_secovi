<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Paulo Sergio Correia">
    <meta name="generator" content="">
    <meta name="docsearch:language" content="en">
    <meta name="docsearch:version" content="5.0">

    <title>Inclusão</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

  </head>
  <div class="container">
    <div class="col-md-6 col-md-offset-0">
      <h3>Inclusão</h3>
      <form class="form-horizontal" role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Nome: <input id="name" type="text" class="form-control" name="name">
        E-mail: <input id="email" type="text" class="form-control" name="email">
        Endereço: <input id="endereco" type="text" class="form-control" name="endereco">
        <br>
      <button type="submit" name="incluir" class="btn btn-primary">Incluir</button>
      </form>

  <?php
  if (isset($_POST['incluir'])) {
    echo "<br>";
    $res=0;
    $data = $_POST;

    if ( (strlen($data['name'])<1) && (strlen($data['email'])<1) && (strlen($data['endereco'])<1) ) {
      echo '<div class="alert alert-danger">';
      echo "Erro, todos os campos estão vazios !!";
      echo '</div>';
      exit;
    }
    if ( (strlen($data['name'])<1) && (strlen($data['email'])>0) && (strlen($data['endereco'])>0) ) {
      echo '<div class="alert alert-danger">';
      echo "Erro, o campo nome está vazio !!";
      echo '</div>';
      exit;
    }

    include "crud.php";

    $host='localhost';
    $name = 'secovi';
    $user = 'root';
    $pass = 'postgres';

    $crud= new Crud($host, $name, $user, $pass);

    if ( (strlen($data['name'])>0) && (strlen($data['email'])<1) && (strlen($data['endereco'])<1) ) {
      $res=$crud->create("pessoas","nome","'".$data['name']."'");
    }

    if ( (strlen($data['name'])>0) && (strlen($data['email'])>0) && (strlen($data['endereco'])<1) ) {
      $crud->create("pessoas","nome","'".$data['name']."'");
      $id = $crud->read("max(id)","pessoas");
      $res = $crud->create("contatos","id_pessoa, email",$id[0]["max"].",'".$data['email']."'");
    }

    if ( (strlen($data['name'])>0) && (strlen($data['email'])>0) && (strlen($data['endereco'])>0) ) {
      $crud->create("pessoas","nome","'".$data['name']."'");
      $id = $crud->read("max(id)","pessoas");
      $crud->create("contatos","id_pessoa, email",$id[0]["max"].",'".$data['email']."'");
      $res = $crud->create("enderecos","id_pessoa, endereco",$id[0]["max"].",'".$data['endereco']."'");
    }

    if ( (strlen($data['name'])>0) && (strlen($data['email'])<1) && (strlen($data['endereco'])>0) ) {
      $crud->create("pessoas","nome","'".$data['name']."'");
      $id = $crud->read("max(id)","pessoas");
      $res = $crud->create("enderecos","id_pessoa, endereco",$id[0]["max"].",'".$data['endereco']."'");
    }

    if ($res == 1) {
      echo '<div class="alert alert-success">';
      echo "Cadastrado com sucesso!!";
      echo '</div>';
    } else {
      echo '<div class="alert alert-danger">';
      echo "Ocorreu um erro no cadastro";
      echo '</div>';
    }

  }
  ?>

</div>
</div>
